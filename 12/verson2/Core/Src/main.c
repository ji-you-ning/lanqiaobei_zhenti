/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file           : main.c
  * @brief          : Main program body
  ******************************************************************************
  * @attention
  *
  * Copyright (c) 2023 STMicroelectronics.
  * All rights reserved.
  *
  * This software is licensed under terms that can be found in the LICENSE file
  * in the root directory of this software component.
  * If no LICENSE file comes with this software, it is provided AS-IS.
  *
  ******************************************************************************
  */
/* USER CODE END Header */
/* Includes ------------------------------------------------------------------*/
#include "main.h"
#include "tim.h"
#include "usart.h"
#include "gpio.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */
#include "lcd.h"
#include "string.h"
#include "parkControl.h"
#include "stdio.h"

/* USER CODE END Includes */

/* Private typedef -----------------------------------------------------------*/
/* USER CODE BEGIN PTD */

/* USER CODE END PTD */

/* Private define ------------------------------------------------------------*/
/* USER CODE BEGIN PD */

/* USER CODE END PD */

/* Private macro -------------------------------------------------------------*/
/* USER CODE BEGIN PM */

/* USER CODE END PM */

/* Private variables ---------------------------------------------------------*/

/* USER CODE BEGIN PV */

/* USER CODE END PV */

/* Private function prototypes -----------------------------------------------*/
void SystemClock_Config(void);
/* USER CODE BEGIN PFP */

/* USER CODE END PFP */

/* Private user code ---------------------------------------------------------*/
/* USER CODE BEGIN 0 */

#define REC_LENGTH  100		//定义接收数据长度，根据使用需求自定义
uint8_t UART1_Rx_Buf[REC_LENGTH];	//接收数据存储
uint8_t UART1_Rx_flg=0;		//定时器中断标志位
uint8_t UART1_Rx_cnt=0;		//串口接收数据下标
uint8_t Rxbuff[1]; //串口接收数据缓冲
//显示的字符串
char showCNBR[10];
char showVNBR[10];
char showIDLE[10];
//接收的字符串
char BUFF[50];

//各种变量
int size=8;
char type[5];
char id[50];
char timetemp[50];
int year;
int month;
int day;
int hour;
int min;
int sec;
int csize=0;
int vsize=0;
double cprize=3.50;
double vprize=2.00;
int key1_count=0;
int key4_count=0;
//结构体

Car car;
Time time;
Parking parking[8];



//车位显示界面
void lcd1()
{
	LCD_Clear(Black);
	LCD_DisplayStringLine(Line1,(u8*) "       Data         ");
	sprintf(showCNBR,"   CNBR:%d",csize);
	LCD_DisplayStringLine(Line3,(u8*)showCNBR);	
	sprintf(showVNBR,"   VNBR:%d",vsize);
	LCD_DisplayStringLine(Line5,(u8*)showVNBR);
	sprintf(showIDLE,"   IDLE:%d",size);
	LCD_DisplayStringLine(Line7,(u8*)showIDLE);		
}
//费率设置界面
void lcd2()
{
	LCD_Clear(Black);
	LCD_DisplayStringLine(Line1,(u8*) "       Para         ");
	sprintf(showCNBR,"   CNBR:%.2f",cprize);
	LCD_DisplayStringLine(Line3,(u8*)showCNBR);
	sprintf(showVNBR,"   VNBR:%.2f",vprize);
	LCD_DisplayStringLine(Line5,(u8*)showVNBR);
	
}

int keynum()
{
	int KeyNum=0;
	if(HAL_GPIO_ReadPin(GPIOB,GPIO_PIN_0)==0)
	{
		HAL_Delay(5);
		if(HAL_GPIO_ReadPin(GPIOB,GPIO_PIN_0)==0)
		{
			KeyNum=1;
		}
		while(HAL_GPIO_ReadPin(GPIOB,GPIO_PIN_0)==0);
	}

	if(HAL_GPIO_ReadPin(GPIOB,GPIO_PIN_1)==0)
	{
		HAL_Delay(5);
		if(HAL_GPIO_ReadPin(GPIOB,GPIO_PIN_1)==0)
		{

			KeyNum=2;
		}
		while(HAL_GPIO_ReadPin(GPIOB,GPIO_PIN_1)==0);
	}

	if(HAL_GPIO_ReadPin(GPIOB,GPIO_PIN_2)==0)
	{
		HAL_Delay(5);
		if(HAL_GPIO_ReadPin(GPIOB,GPIO_PIN_2)==0)
		{

			KeyNum=3;
		}
		while(HAL_GPIO_ReadPin(GPIOB,GPIO_PIN_2)==0);
	}

	if(HAL_GPIO_ReadPin(GPIOA,GPIO_PIN_0)==0)
	{
		HAL_Delay(5);
		if(HAL_GPIO_ReadPin(GPIOA,GPIO_PIN_0)==0)
		{
			KeyNum=4;
		}
		while(HAL_GPIO_ReadPin(GPIOA,GPIO_PIN_0)==0);
	}
	return KeyNum;
}


void led(int x,int a)   //x:1-8 对应8个灯   a：1开 0灭
{
	int pin;
	switch(x)
	{
		case 1:pin=GPIO_PIN_8;break;
		case 2:pin=GPIO_PIN_9;break;
		case 3:pin=GPIO_PIN_10;break;
		case 4:pin=GPIO_PIN_11;break;
		case 5:pin=GPIO_PIN_12;break;
		case 6:pin=GPIO_PIN_13;break;
		case 7:pin=GPIO_PIN_14;break;
		case 8:pin=GPIO_PIN_15;break;
  }
	
	if(a==0) HAL_GPIO_WritePin(GPIOC, pin, GPIO_PIN_SET);
	if(a==1) HAL_GPIO_WritePin(GPIOC, pin, GPIO_PIN_RESET);
//  HAL_GPIO_WritePin(GPIOD, GPIO_PIN_2, 0);//开关一次锁存器,对led的配置才有效	
//  HAL_GPIO_WritePin(GPIOD, GPIO_PIN_2, 1);//开关一次锁存器,对led的配置才有效

}
//是否有空闲位置
void idle()
{
	if((8-csize-vsize)>0)
	{
		led(1,1);
	}
	led(1,0);
}


//按键实现的功能
void key_function()
{
	int key=keynum();
	if(key==1)
	{
		if(key1_count%2==1)
		{
			lcd1();
			key1_count++;
		}
		else
		{
			lcd2();
			key1_count++;
		}
	}
	if(key==2)
	{
		if(key1_count%2==1)
		{
			cprize=cprize+0.5;
			vprize=vprize+0.5;
			lcd2();
		}
	}
	if(key==3)
	{
		if(key1_count%2==1)
		{
			cprize=cprize-0.5;
			vprize=vprize-0.5;
			lcd2();
		}
	}
	if(key==4)
	{
		if(key4_count%2==1)
		{
			__HAL_TIM_SET_COMPARE(&htim3, TIM_CHANNEL_2, 20);
			led(2,1);
			key4_count++;
		}
		else
		{
			__HAL_TIM_SET_COMPARE(&htim3, TIM_CHANNEL_2, 0);
			led(2,0);
			key4_count++;
		}
	}
	
}


//找到停车场已有该车的具体位置
int location;

//回传给电脑数据
char tsdata[50];

//初始化汽车
//这个因为type和id是数组，所以是指针传递，注意与下面注释的进行对比，注释的是值传递
void initCar(char*f1,char*f2)
{
	strcpy(f1,type);
	strcpy(f2,id);	
}

////初始化时间
//默认情况下，函数参数是通过值传递的。这意味着当你调用 initTime 函数时
//你传递的是 Time 结构体中各成员的值的副本，而不是这些成员本身的引用或指针。
//所以不能这样调用，这是我发生的错误
//void initTime(int Year,int Month,int Day,int Hour,int Min,int Sec)
//{
//	Year=year;
//	Month=month;	
//	Day=day;
//	Hour=hour;
//	Min=min;
//	Sec=sec;
//}


//初始化时间

//方法1指针调用
//Time* time，initTime(time);
//要把所有形参是Time time改成Time*time，parking[i].time.year也需要改成parking[i].time->year等等
//void initTime(Time*time)
//{
//	time->year=year;
//	time->month=month;	
//	time->day=day;
//	time->hour=hour;
//	time->min=min;
//	time->sec=sec;
//}


//方法2-有返回值函数
Time initTime()
{
	Time time;
	time.year = year;
	time.month = month;
	time.day = day;
	time.hour = hour;
	time.min = min;
	time.sec = sec;
	return time;
}

//初始化停车场
void initParking(Parking*parking)
{
	for(int i=0;i<8;i++)
	{
		strcpy(parking[i].car.type,"");
		strcpy(parking[i].car.id,"");
		parking[i].time.year=0;
		parking[i].time.month=0;
		parking[i].time.day=0;
		parking[i].time.hour=0;
		parking[i].time.min=0;
		parking[i].time.sec=0;
		parking[i].canpark=1;
	}

}

//字符串切割
void analysis(char *orgin,char *later,int pos,int length)
{
	int i;
	for(i=0;i<length;i++)
	{
		later[i]=orgin[i+pos];
	}
	later[length]='\0';
}

//解析数据
//int judge(char*data)
//{
//	if(strlen(data)!=22)
//	{
//		HAL_UART_Transmit(&huart1, (u8*)"Error Type Format1\n", 19, 200);
//		return 0;
//	}
//	return 1;
//	
//}

int handle(char*data)
{
	if(strlen(data)!=22)
	{
		
	HAL_UART_Transmit(&huart1, (u8*)"Error Type Format1\n", 19, 200);
		return 0;
	}

	analysis(data, type, 0, 4);
	analysis(data, id,5,4);
	// 提取年份并转换为整数
	analysis(data, timetemp, 10, 2);
	//atoi函数将字符串转换为整数
	year = atoi(timetemp);

	// 提取月份并转换为整数
	analysis(data, timetemp, 12, 2);
	month = atoi(timetemp);

	// 提取日并转换为整数
	analysis(data, timetemp, 14, 2);
	day = atoi(timetemp);

	// 提取小时并转换为整数
	analysis(data, timetemp, 16, 2);
	hour = atoi(timetemp);
//	sprintf(h,"%d",hour);
//	HAL_UART_Transmit(&huart1, h, 2, 100);
	// 提取分钟并转换为整数
	analysis(data, timetemp, 18, 2);
	min = atoi(timetemp);

	// 提取秒并转换为整数
	analysis(data, timetemp, 20, 2);
	sec = atoi(timetemp);
	//清空接收区
	memset(timetemp,0,sizeof(timetemp));
	memset(data,0,strlen(data));
	if(!(strcmp(type,"VNBR")==0 || strcmp(type,"CNBR")==0))
	{
		HAL_UART_Transmit(&huart1, (u8*)"Error Type Format2\n", 19, 200);
		return 0;
	}
	else if(month>12 || day>31 || hour>24 || min>60 || sec>60 )
	{
		HAL_UART_Transmit(&huart1, (u8*)"Error Type Format3\n", 19, 200);
		return 0;
	}

	return 1;
}


void HAL_UART_RxCpltCallback(UART_HandleTypeDef *huart)
{
	
    // 当接收到一个字节时，这个函数会被调用

    if (0 == UART1_Rx_cnt) {
        // 如果这是接收的第一个字节，重置定时器并启动
        __HAL_TIM_CLEAR_FLAG(&htim1, TIM_FLAG_UPDATE);
        HAL_TIM_Base_Start_IT(&htim1);
    }

    // 重置定时器计数器
    __HAL_TIM_SET_COUNTER(&htim1, 0);

    // 将接收到的字节存入接收缓冲区
    UART1_Rx_Buf[UART1_Rx_cnt] = Rxbuff[0];
    // 接收字节计数器加一
    UART1_Rx_cnt++;
	UART1_Rx_Buf[UART1_Rx_cnt]='\0';
    // 重新启用UART接收中断，准备接收下一个字节
    HAL_UART_Receive_IT(&huart1, Rxbuff, 1);

}

void HAL_TIM_PeriodElapsedCallback(TIM_HandleTypeDef *htim) // 这个函数在定时器时间到达时被调用
{
    if (htim == (&htim1)) {
        // 如果是我们关心的定时器（htim1）触发了中断

        // 设置接收标志，表示数据接收完成或超时
        UART1_Rx_flg = 1;
        // 停止定时器中断
        HAL_TIM_Base_Stop_IT(&htim1);
    }
}

/* USER CODE END 0 */

/**
  * @brief  The application entry point.
  * @retval int
  */
int main(void)
{
  /* USER CODE BEGIN 1 */

  /* USER CODE END 1 */

  /* MCU Configuration--------------------------------------------------------*/

  /* Reset of all peripherals, Initializes the Flash interface and the Systick. */
  HAL_Init();

  /* USER CODE BEGIN Init */

  /* USER CODE END Init */

  /* Configure the system clock */
  SystemClock_Config();

  /* USER CODE BEGIN SysInit */

  /* USER CODE END SysInit */

  /* Initialize all configured peripherals */
  MX_GPIO_Init();
  MX_USART1_UART_Init();
  MX_TIM3_Init();
  MX_TIM17_Init();
  MX_TIM1_Init();
  /* USER CODE BEGIN 2 */
  LCD_Init();

  initParking(parking);
	HAL_UART_Receive_IT(&huart1, (uint8_t*)Rxbuff, 1);
	
	LCD_Clear(Black);
	LCD_SetTextColor(White);
    LCD_SetBackColor(Black);
	lcd1();
	HAL_TIM_PWM_Start(&htim3, TIM_CHANNEL_2);
	led(1,0);
	led(2,0);
	led(3,0);
	led(4,0);
	led(5,0);
	led(6,0);
	led(7,0);
	led(8,0);
		  
  /* USER CODE END 2 */

  /* Infinite loop */
  /* USER CODE BEGIN WHILE */
  while (1)
  {
    /* USER CODE END WHILE */

    /* USER CODE BEGIN 3 */
	  	if (UART1_Rx_flg) // 如果接收标志被设置（表示数据接收完成或超时）
		{
			// 通过UART发送接收到的数据
//			HAL_UART_Transmit(&huart1, UART1_Rx_Buf, UART1_Rx_cnt, 0xffff); //65535
			// 清空接收缓冲区
			// 重置接收字节计数器
			UART1_Rx_cnt = 0;
			// 重置接收标志
			UART1_Rx_flg = 0;
				//当且仅当数据格式正确的时候
			if(handle(UART1_Rx_Buf))
			{
				//初始化车和时间
				initCar(car.type,car.id);
				
				time=initTime();
				
				parkControl(car,time,parking);
			}

			memset(UART1_Rx_Buf,0,sizeof(UART1_Rx_Buf));
//			for (int i = 0; i < UART1_Rx_cnt; i++)
//			{
//				UART1_Rx_Buf[i] = 0;				
//			}

		}
		
		key_function();
		idle();

	  
  }
  /* USER CODE END 3 */
}

/**
  * @brief System Clock Configuration
  * @retval None
  */
void SystemClock_Config(void)
{
  RCC_OscInitTypeDef RCC_OscInitStruct = {0};
  RCC_ClkInitTypeDef RCC_ClkInitStruct = {0};

  /** Configure the main internal regulator output voltage
  */
  HAL_PWREx_ControlVoltageScaling(PWR_REGULATOR_VOLTAGE_SCALE1);

  /** Initializes the RCC Oscillators according to the specified parameters
  * in the RCC_OscInitTypeDef structure.
  */
  RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSI;
  RCC_OscInitStruct.HSIState = RCC_HSI_ON;
  RCC_OscInitStruct.HSICalibrationValue = RCC_HSICALIBRATION_DEFAULT;
  RCC_OscInitStruct.PLL.PLLState = RCC_PLL_ON;
  RCC_OscInitStruct.PLL.PLLSource = RCC_PLLSOURCE_HSI;
  RCC_OscInitStruct.PLL.PLLM = RCC_PLLM_DIV1;
  RCC_OscInitStruct.PLL.PLLN = 10;
  RCC_OscInitStruct.PLL.PLLP = RCC_PLLP_DIV2;
  RCC_OscInitStruct.PLL.PLLQ = RCC_PLLQ_DIV2;
  RCC_OscInitStruct.PLL.PLLR = RCC_PLLR_DIV2;
  if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK)
  {
    Error_Handler();
  }

  /** Initializes the CPU, AHB and APB buses clocks
  */
  RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_HCLK|RCC_CLOCKTYPE_SYSCLK
                              |RCC_CLOCKTYPE_PCLK1|RCC_CLOCKTYPE_PCLK2;
  RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_PLLCLK;
  RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
  RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV1;
  RCC_ClkInitStruct.APB2CLKDivider = RCC_HCLK_DIV1;

  if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_2) != HAL_OK)
  {
    Error_Handler();
  }
}

/* USER CODE BEGIN 4 */

/* USER CODE END 4 */

/**
  * @brief  This function is executed in case of error occurrence.
  * @retval None
  */
void Error_Handler(void)
{
  /* USER CODE BEGIN Error_Handler_Debug */
  /* User can add his own implementation to report the HAL error return state */
  __disable_irq();
  while (1)
  {
  }
  /* USER CODE END Error_Handler_Debug */
}

#ifdef  USE_FULL_ASSERT
/**
  * @brief  Reports the name of the source file and the source line number
  *         where the assert_param error has occurred.
  * @param  file: pointer to the source file name
  * @param  line: assert_param error line source number
  * @retval None
  */
void assert_failed(uint8_t *file, uint32_t line)
{
  /* USER CODE BEGIN 6 */
  /* User can add his own implementation to report the file name and line number,
     ex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */
  /* USER CODE END 6 */
}
#endif /* USE_FULL_ASSERT */
