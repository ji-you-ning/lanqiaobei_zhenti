#include "string.h"
#include "stdlib.h"
#include "usart.h"

//结构体
//汽车
typedef struct Car
{
	char type[5];
	char id[5];
	
}Car;
//时间
typedef struct time
{
	int year;
	int month;
	int hour;
	int day;
	int min;
	int sec;
	
}Time;

//停车场
typedef struct parking
{
	Car car;
	Time time;
	int canpark;
	
}Parking;

//是否停车场里是否有该车，有返回1
int Rename(Car car,Time time,Parking*parking);
//入库,成功入库返回1
int instation(Car car,Time time,Parking*parking);
//出库时的一系列操作
void outstation(Car car,Time time,Parking*parking);
//停车控制流程
void parkControl(Car car,Time time,Parking*parking);