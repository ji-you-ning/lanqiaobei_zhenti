#include "string.h"
#include "stdlib.h"
#include "usart.h"

//结构体
//汽车
typedef struct Car
{
	char type[5];
	char id[5];
	
}Car;
//时间
typedef struct time
{
	int year;
	int month;
	int hour;
	int day;
	int min;
	int sec;
	
}Time;

//停车场
typedef struct parking
{
	Car car;
	Time time;
	int canpark;
	
}Parking;

