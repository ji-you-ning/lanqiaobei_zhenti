#include "parkControl.h"
#include "string.h"
#include "stdio.h"
#include "main.h"
#include "lcd.h"
extern int size;
extern int location;
char local[1];
char h[4];
extern char tsdata[50];

extern int csize;
extern int vsize;
extern double cprize;
extern double vprize;
//是否停车场里是否有该车，有返回1
int Rename(Car car,Time time,Parking*parking)
{

	for(int i=0;i<8;i++)
	{

		if((strcmp(car.type,parking[i].car.type)==0)&&(strcmp(car.id,parking[i].car.id)==0))
		{

			location=i;
//			sprintf(local,"%d",location);
//		HAL_UART_Transmit(&huart1, local, 1, 100);
			return 1;
		}
	}
	return 0;
}

//入库,成功入库返回1
int instation(Car car,Time time,Parking*parking)
{
//				sprintf(local,"%d",location);
//		HAL_UART_Transmit(&huart1, local, 1, 100);
	int temp=Rename(car,time,parking);
	if(temp==0)
	{
		for(int i=0;i<8;i++)
		{
			//判断能否停车
			if(parking[i].canpark)
			{

				if(strcmp(car.type,"CNBR")==0)
				{
					csize++;
				}
				else
				{
					vsize++;
				}
				strcpy(parking[i].car.type,car.type);
				strcpy(parking[i].car.id,car.id);

				parking[i].time.year=time.year;
				parking[i].time.month=time.month;
				parking[i].time.day=time.day;
				parking[i].time.hour=time.hour;
				parking[i].time.min=time.min;
				parking[i].time.sec=time.sec;
				parking[i].canpark=0;

				size--;
				LCD_Clear(Black);
				lcd1();
				return 1;
			}
		}
	}
	return 0;
	
}	

//出库时的一系列操作
void outstation(Car car,Time time,Parking*parking)
{
//			location=5;
//			sprintf(local,"%d",location);
//		HAL_UART_Transmit(&huart1, local, 1, 100);
	double price,sum;
	int hours;
	if(!strcmp(car.type,"CNBR"))
	{
		csize--;
		price=cprize;
	}
	else
	{
		vsize--;
		price=vprize;		
	}
	hours=(time.year-parking[location].time.year)*12*30*24+(time.month-parking[location].time.month)*30*24+(time.day -parking[location].time.day)*24+(time.hour -parking[location].time.hour);

	
	if((time.min-parking[location].time.min)*60+(time.sec-parking[location].time.sec)>0)
	{
		hours++;
	}
	
	
	if(hours<0)
	{
		sprintf(tsdata,"error invalid time");
	}
	else
	{
		sum=price*hours;
		sprintf(tsdata,"%s:%s:%d:%.2f",car.type,car.id,hours,sum);

	}
		HAL_UART_Transmit(&huart1, (uint8_t*)tsdata,18 , 100);
		//清空发送区
		memset(tsdata,0,sizeof(tsdata));
		//出库
		strcpy(parking[location].car.type,"");
		strcpy(parking[location].car.id,"");
		parking[location].time.year=0;
		parking[location].time.month=0;
		parking[location].time.day=0;
		parking[location].time.hour=0;
		parking[location].time.min=0;
		parking[location].time.sec=0;
		parking[location].canpark=1;
		size++;
		LCD_Clear(Black);
		lcd1();

}


void parkControl(Car car,Time time,Parking*parking)
{
//				sprintf(h,"%d",time.hour);
//		HAL_UART_Transmit(&huart1, h, 2, 100);

	if(instation(car,time,parking)==0)
	{
		outstation(car,time,parking);
	}
}
