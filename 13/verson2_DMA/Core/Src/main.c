/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file           : main.c
  * @brief          : Main program body
  ******************************************************************************
  * @attention
  *
  * Copyright (c) 2024 STMicroelectronics.
  * All rights reserved.
  *
  * This software is licensed under terms that can be found in the LICENSE file
  * in the root directory of this software component.
  * If no LICENSE file comes with this software, it is provided AS-IS.
  *
  ******************************************************************************
  */
/* USER CODE END Header */
/* Includes ------------------------------------------------------------------*/
#include "main.h"
#include "dma.h"
#include "tim.h"
#include "usart.h"
#include "gpio.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */
#include "lcd.h"

#define MAX 100
char rx_buffer[MAX];//接收数组
char rx_len = 0; //接收到的数据长度

extern int systick;
int PWMTick;
int led_flag=0;

void passward();
void show_input();
void show_output();
void pwm2();
int key4_count=0;

int judge()
{
	for(int i=0;i<6;i++)
	{
		if((rx_buffer[i]<'0')||(rx_buffer[i]>'9'))//接收数据不为阿拉伯数字时不合法
			return 0;
		else
		{
			return 1;
		}
	}
}

void delay_us(int delay)
{
	HAL_TIM_Base_Start(&htim3);        // 启动定时器
 	 __HAL_TIM_SET_COUNTER(&htim3, 0);  // 重置计数器
  	while (__HAL_TIM_GET_COUNTER(&htim3) < delay)
	  {}
	 HAL_TIM_Base_Stop(&htim3);         // 停止定时器
}


void led(int a,int b)
{
	int pin;
	switch(a)
	{
		case 1:pin=GPIO_PIN_8;break;
		case 2:pin=GPIO_PIN_9;break;
		case 3:pin=GPIO_PIN_10;break;
		case 4:pin=GPIO_PIN_11;break;
		case 5:pin=GPIO_PIN_12;break;
		case 6:pin=GPIO_PIN_13;break;
		case 7:pin=GPIO_PIN_14;break;
		case 8:pin=GPIO_PIN_15;break;
	}
	HAL_GPIO_WritePin(GPIOC,pin,!b);
	
	HAL_GPIO_WritePin(GPIOD,GPIO_PIN_2,GPIO_PIN_SET);	
	HAL_GPIO_WritePin(GPIOD,GPIO_PIN_2,GPIO_PIN_RESET);
}


int Keynum()
{
	int key=0;
	if(HAL_GPIO_ReadPin(GPIOB, GPIO_PIN_0)==0)
	{
		HAL_Delay(5);
		if(HAL_GPIO_ReadPin(GPIOB, GPIO_PIN_0)==0)
		{
			key=1;
			  LCD_Clear(Black);
		}
		while(HAL_GPIO_ReadPin(GPIOB, GPIO_PIN_0)==0);
	}
	
	if(HAL_GPIO_ReadPin(GPIOB, GPIO_PIN_1)==0)
	{
		HAL_Delay(5);
		if(HAL_GPIO_ReadPin(GPIOB, GPIO_PIN_1)==0)
		{
			key=2;
			  LCD_Clear(Black);
		}
		while(HAL_GPIO_ReadPin(GPIOB, GPIO_PIN_1)==0);
	}
	
	if(HAL_GPIO_ReadPin(GPIOB, GPIO_PIN_2)==0)
	{
		HAL_Delay(5);
		if(HAL_GPIO_ReadPin(GPIOB, GPIO_PIN_2)==0)
		{
			key=3;
			  LCD_Clear(Black);
		}
		while(HAL_GPIO_ReadPin(GPIOB, GPIO_PIN_2)==0);
	}
	
	if(HAL_GPIO_ReadPin(GPIOA, GPIO_PIN_0)==0)
	{
		HAL_Delay(5);
		if(HAL_GPIO_ReadPin(GPIOA, GPIO_PIN_0)==0)
		{
			key=4;
			 LCD_Clear(Black);
		}
		while(HAL_GPIO_ReadPin(GPIOA, GPIO_PIN_0)==0);
	}
	
	return key;
}



char first='@';
int first_flag=-1;

char second='@';
int second_flag=-1;

char third='@';
int third_flag=-1;

char pass1='1';
char pass2='2';
char pass3='3';
int pass_flag=0;

int lcd_flag=0;

int ledTick=0;
void Key_handle()
{
	int keynum=Keynum();
	if(keynum==1)
	{
			first_flag++;
			switch(first_flag%10)
			{
				case 0:first='0';break;
				case 1:first='1';break;
				case 2:first='2';break;
				case 3:first='3';break;
				case 4:first='4';break;
				case 5:first='5';break;
				case 6:first='6';break;
				case 7:first='7';break;
				case 8:first='8';break;
				case 9:first='9';break;
			}
	}
	
	if(keynum==2)
	{
			second_flag++;
			switch(second_flag%10)
			{
				case 0:second='0';break;
				case 1:second='1';break;
				case 2:second='2';break;
				case 3:second='3';break;
				case 4:second='4';break;
				case 5:second='5';break;
				case 6:second='6';break;
				case 7:second='7';break;
				case 8:second='8';break;
				case 9:second='9';break;
			}

	}
	
	
	if(keynum==3)
	{
			third_flag++;
			switch(third_flag%10)
			{
				case 0:third='0';break;
				case 1:third='1';break;
				case 2:third='2';break;
				case 3:third='3';break;
				case 4:third='4';break;
				case 5:third='5';break;
				case 6:third='6';break;
				case 7:third='7';break;
				case 8:third='8';break;
				case 9:third='9';break;
			}
	}

	if(keynum==4)
	{
		led_flag=0;
		key4_count++;
		if(pass1==first&&pass2==second&&pass3==third)
		{
			key4_count=0;
			pass_flag=1;
			pwm2();
			led(1,1);
			//定时器中断在回调函数中清屏函数不起作用
//			__HAL_TIM_CLEAR_FLAG(&htim1,TIM_FLAG_UPDATE);
//		    HAL_TIM_Base_Start_IT(&htim1);
			PWMTick=systick;		
		}
		else
		{
			pass_flag=0;
			first='@';
			first_flag=-1;

			second='@';
			second_flag=-1;

			third='@';
			third_flag=-1;
		}
		
		if(key4_count>=3)
		{
			__HAL_TIM_CLEAR_FLAG(&htim4,TIM_FLAG_UPDATE);
		    HAL_TIM_Base_Start_IT(&htim4);
		}
	}
}
void pwm2()
{
	__HAL_TIM_SET_AUTORELOAD(&htim2,50-1);
	__HAL_TIM_SET_COMPARE(&htim2,TIM_CHANNEL_2,5);
}

void pwm1()
{
	__HAL_TIM_SET_AUTORELOAD(&htim2,100-1);
	__HAL_TIM_SET_COMPARE(&htim2,TIM_CHANNEL_2,50);
}


void lcd()
{
	if(pass_flag==1)
	{
		show_output();	
		if(systick-PWMTick>5000)
		{
			led(1,0);
			pwm1();
			first='@';
			first_flag=-1;

			second='@';
			second_flag=-1;

			third='@';
			third_flag=-1;
		
			pass_flag=0;
			LCD_Clear(Black);
		}	
	}
	if(pass_flag==0)
	{
		show_input();		
	}
}


char B1[50];
char B2[50];
char B3[50];
void show_input()
{
	LCD_DisplayStringLine(Line1,"       PSD             ");
	sprintf(B1,"    B1:%c",first);
	LCD_DisplayStringLine(Line3,B1);
	sprintf(B2,"    B2:%c",second);
	LCD_DisplayStringLine(Line4,B2);
	sprintf(B3,"    B3:%c",third);
	LCD_DisplayStringLine(Line5,B3);
}

char pl[50];
char zkb[50];
int c_pl;
int c_ARR;
int c_zkb;
int c_pluse;
void show_output()
{
	LCD_DisplayStringLine(Line1,"       STA             ");
	c_ARR=__HAL_TIM_GET_AUTORELOAD(&htim2);
	c_pl=100000/(c_ARR+1);
	sprintf(pl,"    F:%dHz",c_pl);
	LCD_DisplayStringLine(Line3,pl);
	
	c_pluse=__HAL_TIM_GET_COMPARE(&htim2,TIM_CHANNEL_2);
	c_zkb=c_pluse*100/(c_ARR+1);
	sprintf(zkb,"    S:%d%%",c_zkb);
	LCD_DisplayStringLine(Line4,zkb);
}


void HAL_TIM_PeriodElapsedCallback(TIM_HandleTypeDef *htim)
{
	if(htim->Instance==TIM1)
	{
			led(1,0);
			pwm1();
			first='@';
			first_flag=-1;

			second='@';
			second_flag=-1;

			third='@';
			third_flag=-1;
		
			pass_flag=0;
			LCD_Clear(Black);
			HAL_TIM_Base_Stop_IT(&htim1);	
	}

	if(htim->Instance==TIM4)
	{
			led_flag=1;
			HAL_TIM_Base_Stop_IT(&htim4);	
	}
}


//void HAL_UART_RxCpltCallback(UART_HandleTypeDef *huart)
//{
//	if(buff[0]==pass1&&buff[1]==pass2&&buff[2]&&buff[3]=='-')
//	{
//		pass1=buff[4];
//		pass2=buff[5];
//		pass3=buff[6];
//		HAL_UART_Transmit(&huart1,(unsigned char *)"Sucess", 6, 50);
//	}
//	HAL_UART_Receive_IT(&huart1, buff, 7);
//}
/* USER CODE END Includes */

/* Private typedef -----------------------------------------------------------*/
/* USER CODE BEGIN PTD */

/* USER CODE END PTD */

/* Private define ------------------------------------------------------------*/
/* USER CODE BEGIN PD */

/* USER CODE END PD */

/* Private macro -------------------------------------------------------------*/
/* USER CODE BEGIN PM */

/* USER CODE END PM */

/* Private variables ---------------------------------------------------------*/

/* USER CODE BEGIN PV */

/* USER CODE END PV */

/* Private function prototypes -----------------------------------------------*/
void SystemClock_Config(void);
/* USER CODE BEGIN PFP */

/* USER CODE END PFP */

/* Private user code ---------------------------------------------------------*/
/* USER CODE BEGIN 0 */

/* USER CODE END 0 */

/**
  * @brief  The application entry point.
  * @retval int
  */
int main(void)
{
  /* USER CODE BEGIN 1 */

  /* USER CODE END 1 */

  /* MCU Configuration--------------------------------------------------------*/

  /* Reset of all peripherals, Initializes the Flash interface and the Systick. */
  HAL_Init();

  /* USER CODE BEGIN Init */

  /* USER CODE END Init */

  /* Configure the system clock */
  SystemClock_Config();

  /* USER CODE BEGIN SysInit */

  /* USER CODE END SysInit */

  /* Initialize all configured peripherals */
  MX_GPIO_Init();
  MX_DMA_Init();
  MX_TIM2_Init();
  MX_USART1_UART_Init();
  MX_TIM3_Init();
  MX_TIM4_Init();
  MX_TIM1_Init();
  /* USER CODE BEGIN 2 */
  LCD_Init();
  LCD_Clear(Black);
  LCD_SetBackColor(Black);
  LCD_SetTextColor(White);
	HAL_TIM_PWM_Start(&htim2,TIM_CHANNEL_2);
	led(1,0);
	led(2,0);
	led(3,0);
	led(4,0);
	led(5,0);
	led(6,0);
	led(7,0);
	led(8,0);
//	HAL_UART_Receive_IT(&huart1, buff, 7);
	HAL_TIM_Base_Start_IT(&htim4);
	HAL_UART_Receive_DMA(&huart1,rx_buffer,100);    // 开启DMA接收
  __HAL_UART_ENABLE_IT(&huart1,UART_IT_IDLE);      // 开启串口的空闲中断
  /* USER CODE END 2 */

  /* Infinite loop */
  /* USER CODE BEGIN WHILE */
  while (1)
  {
    /* USER CODE END WHILE */

    /* USER CODE BEGIN 3 */

	  Key_handle();
	  lcd();
	  if(key4_count>=3&&led_flag==0)
	  {

		led(2,1);
		delay_us(50000);
		delay_us(50000);
		led(2,0);
		delay_us(50000);
		delay_us(50000);
	  }
//	  sprintf(eee,"%d",count3);
//		LCD_DisplayStringLine(Line1,eee);
	  

  }
  /* USER CODE END 3 */
}

/**
  * @brief System Clock Configuration
  * @retval None
  */
void SystemClock_Config(void)
{
  RCC_OscInitTypeDef RCC_OscInitStruct = {0};
  RCC_ClkInitTypeDef RCC_ClkInitStruct = {0};

  /** Configure the main internal regulator output voltage
  */
  HAL_PWREx_ControlVoltageScaling(PWR_REGULATOR_VOLTAGE_SCALE1);

  /** Initializes the RCC Oscillators according to the specified parameters
  * in the RCC_OscInitTypeDef structure.
  */
  RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSE;
  RCC_OscInitStruct.HSEState = RCC_HSE_ON;
  RCC_OscInitStruct.PLL.PLLState = RCC_PLL_ON;
  RCC_OscInitStruct.PLL.PLLSource = RCC_PLLSOURCE_HSE;
  RCC_OscInitStruct.PLL.PLLM = RCC_PLLM_DIV3;
  RCC_OscInitStruct.PLL.PLLN = 20;
  RCC_OscInitStruct.PLL.PLLP = RCC_PLLP_DIV2;
  RCC_OscInitStruct.PLL.PLLQ = RCC_PLLQ_DIV2;
  RCC_OscInitStruct.PLL.PLLR = RCC_PLLR_DIV2;
  if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK)
  {
    Error_Handler();
  }

  /** Initializes the CPU, AHB and APB buses clocks
  */
  RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_HCLK|RCC_CLOCKTYPE_SYSCLK
                              |RCC_CLOCKTYPE_PCLK1|RCC_CLOCKTYPE_PCLK2;
  RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_PLLCLK;
  RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
  RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV1;
  RCC_ClkInitStruct.APB2CLKDivider = RCC_HCLK_DIV1;

  if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_2) != HAL_OK)
  {
    Error_Handler();
  }
}

/* USER CODE BEGIN 4 */

/* USER CODE END 4 */

/**
  * @brief  This function is executed in case of error occurrence.
  * @retval None
  */
void Error_Handler(void)
{
  /* USER CODE BEGIN Error_Handler_Debug */
  /* User can add his own implementation to report the HAL error return state */
  __disable_irq();
  while (1)
  {
  }
  /* USER CODE END Error_Handler_Debug */
}

#ifdef  USE_FULL_ASSERT
/**
  * @brief  Reports the name of the source file and the source line number
  *         where the assert_param error has occurred.
  * @param  file: pointer to the source file name
  * @param  line: assert_param error line source number
  * @retval None
  */
void assert_failed(uint8_t *file, uint32_t line)
{
  /* USER CODE BEGIN 6 */
  /* User can add his own implementation to report the file name and line number,
     ex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */
  /* USER CODE END 6 */
}
#endif /* USE_FULL_ASSERT */
